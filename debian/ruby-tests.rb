$: << 'tests' 

disabled_tests=[
  # some tests requiring network access
  'tests/test_idle_connection.rb',
  'tests/test_get_sock_opt.rb',
  'tests/test_set_sock_opt.rb',
  # some tests requiring localhost:9999 to be free, otherwise build blocks
  'tests/test_httpclient2.rb',
  'tests/test_connection_count.rb',
]



(Dir['{tests}/**/*.rb']-disabled_tests).each { |f| require f }
